$(document).ready(function () {
	$("#idade").on("blur", function () {
		if ($(this).val() < 18) {
			$("#menor").show();
			$(".menor").prop('required',true);
			$("#maior").hide();
			$(".maior").prop('required',false);
			$(".maior").val(null);
		} else {
			$("#menor").hide();
			$(".menor").prop('required',false);
			$(".menor").val(null);
			$("#maior").show();
			$(".maior").prop('required',true);
		}
	});

	$(".nivel").on("change", function () {
		if($(this).val() == "ini"){
			$("#apr").hide();
			$("#apr input").val(null);
		} else {
			$("#apr").show();
		}
	});

	$(".q1").on('click', function () {
		if ($(this).val() == "S") {
			$("#q1").show();
		} else {
			$("#q1").hide();
		}
	});

	$(".q2").on('click', function () {
		if ($(this).val() == "S") {
			$("#q2").show();
		} else {
			$("#q2").hide();
		}
	});

	$(".q5").on('click', function () {
		if ($(this).val() == "S") {
			$("#q5").show();
		} else {
			$("#q5").hide();
		}
	});

});