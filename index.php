<!DOCTYPE html>
<html>
<head>
	<title>Code4All</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script type="text/javascript" src="script/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="script/scripts.js"></script>
</head>
<body>
	<header>
		<center>
			<nav>
				<a class="btm" href="index.php">CADASTRAR</a>
				<a class="btm" href="entrar.php">ENTRAR</a>
			</nav>
		</center>
	</header>
	<section class="main">
		<form method="post" action="chave.php">
			<table cellspacing="10" style="width: 100%; margin: auto;">
				<tr>
					<td><label>Nome:</label><input type="text" required="required" name="nome" id="nome"></td>
					<td><label>Telefone:</label><input type="text" required="required" name="telefone" id="telefone"></td>
					<td>
						<label>Idade:</label>
						<input type="number" required="required" name="idade" id="idade">
					</td>
					<td><label>Email:</label><input type="email" required="required" name="email" id="email"></td>					
				</tr>
				<tr>
					<td>
						<label>Estado:</label>
						<select required="required" name="estados">
							<option value="AC">Acre</option>
							<option value="AL">Alagoas</option>
							<option value="AP">Amapá</option>
							<option value="AM">Amazonas</option>
							<option value="BA">Bahia</option>
							<option value="CE">Ceará</option>
							<option value="DF">Distrito Federal</option>
							<option value="ES">Espírito Santo</option>
							<option value="GO">Goiás</option>
							<option value="MA">Maranhão</option>
							<option value="MT">Mato Grosso</option>
							<option value="MS">Mato Grosso do Sul</option>
							<option value="MG">Minas Gerais</option>
							<option value="PA">Pará</option>
							<option value="PB">Paraíba</option>
							<option value="PR">Paraná</option>
							<option value="PE">Pernambuco</option>
							<option value="PI">Piauí</option>
							<option value="RJ">Rio de Janeiro</option>
							<option value="RN">Rio Grande do Norte</option>
							<option value="RS">Rio Grande do Sul</option>
							<option value="RO">Rondônia</option>
							<option value="RR">Roraima</option>
							<option value="SC">Santa Catarina</option>
							<option value="SP">São Paulo</option>
							<option value="SE">Sergipe</option>
							<option value="TO">Tocantins</option>
						</select>
					</td>					
					<td><label>Cidade:</label><input type="text" required="required" name="cidade" id="cidade"></td>					
					<td><label>Endereço:</label><input type="text" required="required" name="endereco" id="endereco"></td>					
					<td>
						<label>Cep:</label>
						<input type="number" name="cep">
					</td>					
				</tr>
				<tr>
					<td colspan="4"><label>O que deseja aprender?</label><textarea required="required" name="obs" id="obs"></textarea></td>
				</tr>
			</table>
			<table cellspacing="10" id="menor" style="width: 100%; display: none;">
				<tr>
					<td><label>Nome do Responsável:</label> <input type="text" required="required" name="nome_resp" class="menor" id="nome_resp"></td>
					<td><label>Telefone do Responsável:</label> <input type="text" required="required" name="tel_resp" class="menor" id="tel_resp"></td>
					<td><label>Email do Responsável:</label> <input type="text" required="required" name="email_resp" class="menor" id="email_resp"></td>
				</tr>
			</table>
			<table cellspacing="10" id="maior" style="width: 100%; display: none;">
				<tr>
					<td><label>Profissão:</label><input type="text" required="required" name="profissao" id="profissao" class="maior"></td>
					<td>
						<label>Qual seu objetivo?</label>
						<select required="required" name="obj" class="maior">
							<option value="pro">Aprendizado próprio</option>
							<option value="ens">Para ensinar</option>
						</select>
					</td>
				</tr>
			</table>
			<table cellspacing="10" style="width: 100%; margin: auto;">
				<td>
					<label>Nível de conhecimeto:</label>
					<select class="nivel" name="nivel">
						<option value="ini">Iniciante</option>
						<option value="med">Médio</option>
						<option value="avn">Avançado</option>
					</select>
				</td>
			</table>
			<table cellspacing="10" style="width: 100%; display: none;" cellspacing="20" id="apr">
				<tr>
					<td>
						<label>Já teve contato com a área?</label>
						Sim: <input type="radio" name="q1" class="q1" value="S">
						Não: <input type="radio" name="q1" class="q1" value="N">
					</td>
				</tr>
				<tr id="q1" style="display: none;">
					<td>
						<label>Detalhe para nós:</label>
						<input type="text" name="detelhe">
					</td>
				</tr>
				<tr>
					<td>
						<label>Você conheçe alguma linguagem de programação?</label>
						Sim: <input type="radio" name="q2" class="q2" value="S">
						Não: <input type="radio" name="q2" class="q2" value="N">
					</td>
				</tr>
				<tr id="q2" style="display: none;">
					<td>
						<label>Qual?</label>
						<input type="text" name="linguagem">
					</td>	
				</tr>
				<tr>
					<td>
						<label>Conheçe os tipos de variáveis? (int, char, float, etc)</label>
						Sim: <input type="radio" name="q3" class="q3" value="S">
						Não: <input type="radio" name="q3" class="q3" value="N">
					</td>
				</tr>
				<tr>
					<td>
						<label>Sabe o que são arrays? (vetores e/ou matrizes)</label>
						Sim: <input type="radio" name="q4" class="q4" value="S">
						Não: <input type="radio" name="q4" class="q4" value="N">
					</td>
				</tr>
				<tr>
					<td>
						<label>Possue algum medo em relação ao mundo da programação?</label>
						Sim: <input type="radio" name="q5" class="q5" value="S">
						Não: <input type="radio" name="q5" class="q5" value="N">
					</td>
				</tr>
				<tr id="q5" style="display: none;">
					<td>
						<label>Qual?</label>
						<input type="text" name="medo">
					</td>
				</tr>
			</table>
			<button class="save" name="cad">Cadastrar</button>
		</form>
	</section>
</body>
</html>